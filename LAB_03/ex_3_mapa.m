clear all
close all
I1 = imread("baboon_512x512.bmp");
I2 = imread("frymire_512x512.bmp");

function hashmap = count_unique(image)
    hashmap = dictionary;
    [x,y,~] = size(image);
    for i = 1:x
        for j = 1:y
            key = squeeze(double(image(i,j,:)));
            key_str = mat2str(key);
            if(i == 1 && j == 1)
                hashmap(key_str) = 0;
            end
            if isKey(hashmap,key_str)
                hashmap(key_str) = hashmap(key_str)+1;
            else
                hashmap(key_str) = 0;
            end
        end
    end
end


function quantized = color_quantization(image, levels)
    [y_s, x_s, channels] = size(image);
    level_size = floor(256 ./ levels);
    quantized = zeros(size(image), 'uint8');
    % for i = 1:256
    %     pallette()
    % end
    v_r = linspace(0,255,levels(1));
    v_g = linspace(0,255,levels(2));
    v_b = linspace(0,255,levels(3));
    rep_r = repelem(v_r,floor(256 ./levels(1)));
    rep_g = repelem(v_g,floor(256 ./levels(2)));
    rep_b = repelem(v_b,floor(256 ./levels(3)));
    [IND,map] = rgb2ind(image,1000);
    map_255 = map.*255;
    map_quant = zeros(255,3);
    for i = 1:size(map,1)
        row_map = map_255(i,:);
        row_map_quant = [rep_r(row_map(1)) rep_g(row_map(2)) rep_b(row_map(3))];
        map_quant(i,:) = row_map_quant ./ 255;

    end
    quantized = ind2rgb(IND,map_quant);
end

I = I1;
subplot(1,4,1);
I_222 = color_quantization(I,[2 2 2]);
imshow(I_222)

hash_222 = count_unique(I_222);
len_1 = length(keys(hash_222));

sub = sprintf("Unikalne kolory: %d",len_1);
title({"[2 2 2]",sub})

subplot(1,4,2);
I_444 = color_quantization(I,[4 4 4]);
imshow(I_444)

hash_444 = count_unique(I_444);
len_2 = length(keys(hash_444));

sub = sprintf("Unikalne kolory: %d",len_2);
title({"[4 4 4]",sub});

subplot(1,4,3);
I_464 = color_quantization(I,[4 6 4]);
imshow(I_464)

hash_464 = count_unique(I_464);
len_3 = length(keys(hash_464));

sub = sprintf("Unikalne kolory: %d",len_3);
title({"[4 6 4]",sub});

subplot(1,4,4);
I_884 = color_quantization(I,[8 8 4]);
imshow(I_884)

hash_884 = count_unique(I_884);
len_4 = length(keys(hash_884));

sub = sprintf("Unikalne kolory: %d",len_4);
title({"[8 8 4]",sub})