clear all
close all
I1 = imread("baboon_512x512.bmp");
I2 = imread("frymire_512x512.bmp");

function hashmap = count_unique(image)
    hashmap = dictionary;
    [x,y,~] = size(image);
    for i = 1:x
        for j = 1:y
            key = squeeze(double(image(i,j,:)));
            key_str = mat2str(key);
            if(i == 1 && j == 1)
                hashmap(key_str) = 0;
            end
            if isKey(hashmap,key_str)
                hashmap(key_str) = hashmap(key_str)+1;
            else
                hashmap(key_str) = 0;
            end
        end
    end
end

tic;
hash = count_unique(I1);
time_1 = toc;
len_1 = length(keys(hash))

tic;
hash_2 = count_unique(I2);
time_2 = toc;

len_2 = length(keys(hash_2))

subplot(1,2,1);
imshow(I1);
counted = sprintf("Liczba barw unikalnych obliczona = %d ",len_1);
title({"O1", "Liczba barw unikalnych POC LAB = 230651", counted, time_1});

subplot(1,2,2);
imshow(I2);
counted = sprintf("Liczba barw unikalnych obliczona = %d ",len_2);
title({"O2", "Liczba barw unikalnych POC LAB = 2276", counted, time_2});

