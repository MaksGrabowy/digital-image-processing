clear all
close all
I1 = imread("baboon_512x512.bmp");
I2 = imread("frymire_512x512.bmp");

function hashmap = count_unique(image)
    hashmap = dictionary;
    [x,y,~] = size(image);
    for i = 1:x
        for j = 1:y
            key = squeeze(double(image(i,j,:)));
            key_str = mat2str(key);
            if(i == 1 && j == 1)
                hashmap(key_str) = 0;
            end
            if isKey(hashmap,key_str)
                hashmap(key_str) = hashmap(key_str)+1;
            else
                hashmap(key_str) = 0;
            end
        end
    end

end

function hashmap = count_unique_hsv(hsv_image)
    hashmap = dictionary;
    [x,y,~] = size(hsv_image);
    for i = 1:x
        for j = 1:y
            H = hsv_image(i,j,1);
            S = hsv_image(i,j,2);
            V = hsv_image(i,j,3);
            key = [H S V];
            key_str = mat2str(key);
            if(i == 1 && j == 1)
                hashmap(key_str) = 0;
            end
            if isKey(hashmap,key_str)
                hashmap(key_str) = hashmap(key_str)+1;
            else
                hashmap(key_str) = 0;
            end
        end
    end

end

hsv_image_1 = rgb2hsv(I1);
imwrite(hsv_image_1,"I1_hsv.bmp");

hash_1_hsv = count_unique_hsv(hsv_image_1);
len_1_hsv = length(keys(hash_1_hsv))

rgb_from_hsv_1 = hsv2rgb(hsv_image_1);
hash_1_rgb = count_unique(rgb_from_hsv_1);
len_1_rgb = length(keys(hash_1_rgb))


hsv_image_2 = rgb2hsv(I2);
imwrite(hsv_image_2,"I2_hsv.bmp");

hash_2_hsv = count_unique_hsv(hsv_image_2);
len_2_hsv = length(keys(hash_2_hsv))

rgb_from_hsv_2 = hsv2rgb(hsv_image_2);
hash_2_rgb = count_unique(rgb_from_hsv_2);
len_2_rgb = length(keys(hash_2_rgb))

counted_hsv = sprintf("Liczba barw unikalnych HSV = %d ",len_1_hsv);
counted_rgb = sprintf("Liczba barw unikalnych RGB = %d ",len_1_rgb);


subplot(1,4,1);
imshow(I1);
title({"O1",counted_rgb , counted_hsv});

subplot(1,4,2);
imshow(rgb_from_hsv_1);
title({"O1RGB",counted_rgb , counted_hsv});

counted_hsv = sprintf("Liczba barw unikalnych HSV = %d ",len_2_hsv);
counted_rgb = sprintf("Liczba barw unikalnych RGB = %d ",len_2_rgb);

subplot(1,4,3);
imshow(I2);
title({"O2",counted_rgb , counted_hsv});

subplot(1,4,4);
imshow(rgb_from_hsv_2);
title({"O2RGB",counted_rgb , counted_hsv});

