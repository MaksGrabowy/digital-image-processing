clear all
close all
I1 = imread("baboon_512x512.bmp");
I2 = imread("frymire_512x512.bmp");


function out = color_quantization_hsv(image_rgb, levels)
    [y_s, x_s, channels] = size(image_rgb);
    level_size = floor(256 ./ levels);
    quantized = zeros(size(image_rgb));
    image_hsv = rgb2hsv(image_rgb);
    for y = 1:y_s
        for x = 1:x_s
            for c = 1:channels
                quant = floor(double(image_hsv(y, x, c)*255) ./ level_size(c)) .* level_size(c) + level_size(c) ./ 2;
                quantized(y, x, c) = min(255, quant); 
            end
        end
    end
    out = hsv2rgb(quantized);
end

figure
I1_222 = color_quantization_hsv(I1,[2 2 2]);
imshow(I1_222)

figure
I1_444 = color_quantization_hsv(I1,[4 4 4]);
imshow(I1_444)

figure
I1_464 = color_quantization_hsv(I1,[4 6 4]);
imshow(I1_464)

figure
I1_884 = color_quantization_hsv(I1,[8 8 4]);
imshow(I1_884)