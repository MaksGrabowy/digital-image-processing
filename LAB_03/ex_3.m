clear all
close all
I1 = imread("baboon_512x512.bmp");
I2 = imread("frymire_512x512.bmp");

function hashmap = count_unique(image)
    hashmap = dictionary;
    [x,y,~] = size(image);
    for i = 1:x
        for j = 1:y
            key = squeeze(double(image(i,j,:)));
            key_str = mat2str(key);
            if(i == 1 && j == 1)
                hashmap(key_str) = 0;
            end
            if isKey(hashmap,key_str)
                hashmap(key_str) = hashmap(key_str)+1;
            else
                hashmap(key_str) = 0;
            end
        end
    end
end


function quantized = color_quantization(image, levels)
    [y_s, x_s, channels] = size(image);
    level_size = floor(256 ./ levels);
    quantized = zeros(size(image), 'uint8');
    for y = 1:y_s
        for x = 1:x_s
            for c = 1:channels
                pixel = floor(double(image(y, x, c)) ./ level_size(c)) .* level_size(c) + level_size(c) ./ 2;
                quantized(y, x, c) = min(255, pixel); 
            end
        end
    end
end

I = I2;
subplot(1,4,1);
I_222 = color_quantization(I,[2 2 2]);
imshow(I_222)

hash_222 = count_unique(I_222);
len_1 = length(keys(hash_222));

sub = sprintf("Unikalne kolory: %d",len_1);
title({"[2 2 2]",sub})

subplot(1,4,2);
I_444 = color_quantization(I,[4 4 4]);
imshow(I_444)

hash_444 = count_unique(I_444);
len_2 = length(keys(hash_444));

sub = sprintf("Unikalne kolory: %d",len_2);
title({"[4 4 4]",sub});

subplot(1,4,3);
I_464 = color_quantization(I,[4 6 4]);
imshow(I_464)

hash_464 = count_unique(I_464);
len_3 = length(keys(hash_464));

sub = sprintf("Unikalne kolory: %d",len_3);
title({"[4 6 4]",sub});

subplot(1,4,4);
I_884 = color_quantization(I,[8 8 4]);
imshow(I_884)

hash_884 = count_unique(I_884);
len_4 = length(keys(hash_884));

sub = sprintf("Unikalne kolory: %d",len_4);
title({"[8 8 4]",sub})