close all
clear all

picture_size = 50:250;

source_image = imread("kodakBus.bmp");
outputFilePathPNG = sprintf('outputs/output_png.png');
imwrite(source_image, outputFilePathPNG, 'png');
[outputImagePNG, map] = imread(outputFilePathPNG);

psnrValuesPNG = psnr(source_image, outputImagePNG);
fileInfoPNG = dir(outputFilePathPNG);
fileInfoBMP = dir("AnisoDialogBox.bmp");
imageSizesPNG = fileInfoPNG.bytes / 1024; % Convert to kilobytes
imageSizesBMP = fileInfoBMP.bytes / 1024;
descriptionBMP = sprintf('No compression (BMP):\n Size: %.2f kB\n PNG (PSNR: %.2f)\n Size: %.2f kB', imageSizesBMP, psnrValuesPNG,imageSizesPNG);
tempImage = imresize(outputImagePNG(picture_size,picture_size,:),4, 'nearest');
part_image_PNG = insertText(tempImage,[5 5], descriptionBMP, 'FontSize', 24,'BoxOpacity', 0.9);
Q1 = [0 25 50 75 100]
for i = 1:5
    outputFilePathJPEG = sprintf('outputs/output_quality_%d_2a.jpg', Q1(i));
    imwrite(source_image, outputFilePathJPEG, 'jpg', 'Quality', Q1(i));

    outputImageJPEG = imread(outputFilePathJPEG);

    psnrValuesJPEG = psnr(source_image, outputImageJPEG);
    ssimValuesJPEG = ssim(source_image, outputImageJPEG);
    fileInfoJPEG = dir(outputFilePathJPEG);
    tempImage = imresize(outputImagePNG(picture_size,picture_size,:),4, 'nearest');
    imageSizesJPEG = fileInfoJPEG.bytes / 1024;

    descriptionJPEG = sprintf('JPEG\n Quality: %d \n PSNR: %.2f \n SSIM: %.2f \n Size: %.2f kB', Q1(i), psnrValuesJPEG, ssimValuesJPEG, imageSizesJPEG);
    tempImage = imresize(outputImageJPEG(picture_size,picture_size,:),4, 'nearest');
    tempImage = insertText(tempImage,[5 5], descriptionJPEG, 'FontSize', 24,'BoxOpacity', 0.9);
    partFilePathJPEG = sprintf('outputs/output_quality_%d_info_2a.jpg', Q1(i));
    imwrite(tempImage, partFilePathJPEG, 'jpg');
end

for i = 1:100
    outputFilePathJPEG = sprintf('temp/output_quality_%d_2a.jpg', i);
    imwrite(source_image, outputFilePathJPEG, 'jpg', 'Quality', i);

    outputImageJPEG = imread(outputFilePathJPEG);

    psnrValuesJPEG = psnr(source_image, outputImageJPEG);
    ssimValuesJPEG = ssim(source_image, outputImageJPEG);
    fileInfoJPEG = dir(outputFilePathJPEG);
    imageSizesJPEG = fileInfoJPEG.bytes / 1024;

    psnr_jpg(i) = psnrValuesJPEG;
    ssim_jpg(i) = ssimValuesJPEG;
    sizes_jpg(i) = imageSizesJPEG;
end


%% GIF
[indexedImage256, map256] = rgb2ind(source_image, 256);
outputFilePathGIF = sprintf('outputs/output_gif.gif');

imwrite(indexedImage256,map256, outputFilePathGIF, 'gif');

[outputImageGIF, map] = imread(outputFilePathGIF);
outputImageGIFRGB = im2uint8(ind2rgb(outputImageGIF,map));

psnrValuesGIF = psnr(source_image, outputImageGIFRGB);
ssimValuesGIF = ssim(source_image, outputImageGIFRGB);
fileInfoGIF = dir(outputFilePathGIF);

imageSizesGIF = fileInfoGIF.bytes / 1024; % Convert to kilobytes
descriptionGIF = sprintf('GIF \nPSNR: %.2f\n SSIM: %.2f \nSize: %.2f kB ', psnrValuesGIF,ssimValuesGIF, imageSizesGIF);

tempImage = imresize(outputImageGIFRGB(picture_size,picture_size,:),4, 'nearest');
part_image_GIF = insertText(tempImage,[5 5], descriptionGIF, 'FontSize', 24,'BoxOpacity', 0.9);
CR = [260 160 20 6 1]
for i = 1:5
    outputFilePathJP2 = sprintf('outputs/output_compression_%d_2a.jp2', CR(i));
    imwrite(source_image, outputFilePathJP2, 'jp2', 'Mode', 'lossy', 'CompressionRatio',CR(i));

    outputImageJP2 = imread(outputFilePathJP2);

    psnrValuesJP2 = psnr(source_image, outputImageJP2);
    ssimValuesJP2 = ssim(source_image, outputImageJP2);
    fileInfoJP2 = dir(outputFilePathJP2);
    tempImage = imresize(outputImageJP2(picture_size,picture_size,:),4, 'nearest');
    imageSizesJP2 = fileInfoJP2.bytes / 1024;

    descriptionJP2 = sprintf('JPEG2000\n CR: %d \n PSNR: %.2f \n SSIM: %.2f \n Size: %.2f kB', CR(i), psnrValuesJP2, ssimValuesJP2, imageSizesJP2);
    tempImage = imresize(outputImageJP2(picture_size,picture_size,:),4, 'nearest');
    tempImage = insertText(tempImage,[5 5], descriptionJP2, 'FontSize', 24,'BoxOpacity', 0.9);
    partFilePathJP2 = sprintf('outputs/output_compression_%d_info_2a.jp2', CR(i));
    imwrite(tempImage, partFilePathJP2, 'jp2');

    psnr_jp2(i) = psnrValuesJP2;
    ssim_jp2(i) = ssimValuesJP2;
    sizes_jp2(i) = imageSizesJP2;
end

for i = 1:5:260
    outputFilePathJP2 = sprintf('temp/output_compression_%d_2a.jp2', i);
    imwrite(source_image, outputFilePathJP2, 'jp2', 'Mode', 'lossy', 'CompressionRatio',i);

    outputImageJP2 = imread(outputFilePathJP2);

    psnrValuesJP2 = psnr(source_image, outputImageJP2);
    ssimValuesJP2 = ssim(source_image, outputImageJP2);
    fileInfoJP2 = dir(outputFilePathJP2);
    imageSizesJP2 = fileInfoJP2.bytes / 1024;

    psnr_jp2(i) = psnrValuesJP2;
    ssim_jp2(i) = ssimValuesJP2;
    sizes_jp2(i) = imageSizesJP2;
end

im_1_2 = imread(sprintf('outputs/output_quality_%d_info_2a.jpg', Q1(1)));
im_1_3 = imread(sprintf('outputs/output_quality_%d_info_2a.jpg', Q1(2)));
im_1_4 = imread(sprintf('outputs/output_quality_%d_info_2a.jpg', Q1(3)));
im_1_5 = imread(sprintf('outputs/output_quality_%d_info_2a.jpg', Q1(4)));
im_1_6 = imread(sprintf('outputs/output_quality_%d_info_2a.jpg', Q1(5)));


im_2_2 = imread(sprintf('outputs/output_compression_%d_info_2a.jp2', CR(1)));
im_2_3 = imread(sprintf('outputs/output_compression_%d_info_2a.jp2', CR(2)));
im_2_4 = imread(sprintf('outputs/output_compression_%d_info_2a.jp2', CR(3)));
im_2_5 = imread(sprintf('outputs/output_compression_%d_info_2a.jp2', CR(4)));
im_2_6 = imread(sprintf('outputs/output_compression_%d_info_2a.jp2', CR(5)));

newImg1 = cat(2, part_image_PNG, im_1_2, im_1_3, im_1_4, im_1_5, im_1_6);
newImg2 = cat(2, part_image_GIF, im_2_2, im_2_3, im_2_4, im_2_5, im_2_6);
res_img = cat(1,newImg1, newImg2);
imshow(res_img)

imwrite(res_img,'outputs/result_image_2a.jpg','jpg');

%%

close all;

figure(1)
plot(sizes_jpg,ssim_jpg,"-x", sizes_jp2, ssim_jp2,"-o");
xlabel("Image size [kB]")
ylabel("SSIM")
legend('JPEG','JPEG2000')
f = gcf;
exportgraphics(f,"jp2_ssim_real.jpg");


close all;

figure(2)
jp2_p = plot(sizes_jpg,psnr_jpg,"-x", sizes_jp2, psnr_jp2,"-o");
xlabel("Image size [kB]")
ylabel("PSNR [dB]")
legend('JPEG','JPEG2000')
f = gcf;
exportgraphics(f,"jp2_psnr_real.jpg");


close all;