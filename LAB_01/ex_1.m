clear all;
close all;
source_image = imread("AnisoDialogBox.bmp");

tempImage = imresize(source_image(50:198,50:198,:),4, 'nearest');
fileInfoBMP = dir("AnisoDialogBox.bmp");
size_BMP = fileInfoBMP.bytes/1024;

description = sprintf('BMP Size: %.2f kB ', size_BMP);

part_image_BMP = insertText(tempImage,[5 5], description, 'FontSize', 24,'BoxOpacity', 0.9);

%% PNG
outputFilePathPNG = sprintf('output_png.png');
imwrite(source_image, outputFilePathPNG, 'png');
[outputImagePNG, map] = imread(outputFilePathPNG);

psnrValuesPNG = psnr(source_image, outputImagePNG);
fileInfoPNG = dir(outputFilePathPNG);
imageSizesPNG = fileInfoPNG.bytes / 1024; % Convert to kilobytes
description = sprintf('PNG (PSNR: %.2f)\nSize: %.2f kB ', psnrValuesPNG, imageSizesPNG);

tempImage = imresize(outputImagePNG(50:198,50:198,:),4, 'nearest');

part_image_PNG = insertText(tempImage,[5 5], description, 'FontSize', 24,'BoxOpacity', 0.9);

%% GIF
[indexedImage256, map256] = rgb2ind(source_image, 256);
outputFilePathGIF = sprintf('output_gif.gif');
imwrite(indexedImage256,map256, outputFilePathGIF, 'gif');
[outputImageGIF, map] = imread(outputFilePathGIF);
outputImageGIFRGB = im2uint8(ind2rgb(outputImageGIF,map));

psnrValuesGIF = psnr(source_image, outputImageGIFRGB);
fileInfoGIF = dir(outputFilePathGIF);
imageSizesGIF = fileInfoGIF.bytes / 1024; % Convert to kilobytes
description = sprintf('GIF (PSNR: %.2f)\nSize: %.2f kB ', psnrValuesGIF, imageSizesGIF);
tempImage = imresize(outputImageGIFRGB(50:198,50:198,:),4, 'nearest');
part_image_GIF = insertText(tempImage,[5 5], description, 'FontSize', 24,'BoxOpacity', 0.9);

%%
res_img_1 = cat(2, part_image_BMP,part_image_PNG, part_image_GIF);
imshow(res_img_1)

%%
source_image_2 = imread("kodakBus.bmp");

tempImage = imresize(source_image_2(50:198,50:198,:),4, 'nearest');
fileInfoBMP = dir("kodakBus.bmp");
size_BMP = fileInfoBMP.bytes/1024;

description = sprintf('BMP Size: %.2f kB ', size_BMP);

part_image_BMP = insertText(tempImage,[5 5], description, 'FontSize', 24,'BoxOpacity', 0.9);

%% PNG
outputFilePathPNG = sprintf('output_png_2.png');
imwrite(source_image_2, outputFilePathPNG, 'png');
[outputImagePNG, map] = imread(outputFilePathPNG);

psnrValuesPNG = psnr(source_image_2, outputImagePNG);
fileInfoPNG = dir(outputFilePathPNG);
imageSizesPNG = fileInfoPNG.bytes / 1024; % Convert to kilobytes
description = sprintf('PNG (PSNR: %.2f)\nSize: %.2f kB ', psnrValuesPNG, imageSizesPNG);

tempImage = imresize(outputImagePNG(50:198,50:198,:),4, 'nearest');

part_image_PNG = insertText(tempImage,[5 5], description, 'FontSize', 24,'BoxOpacity', 0.9);


%% GIF
[indexedImage256, map256] = rgb2ind(source_image_2, 256);
outputFilePathGIF = sprintf('output_gif_2.gif');
imwrite(indexedImage256,map256, outputFilePathGIF, 'gif');
[outputImageGIF, map] = imread(outputFilePathGIF);
outputImageGIFRGB = im2uint8(ind2rgb(outputImageGIF,map));

psnrValuesGIF = psnr(source_image_2, outputImageGIFRGB);
fileInfoGIF = dir(outputFilePathGIF);
imageSizesGIF = fileInfoGIF.bytes / 1024; % Convert to kilobytes
description = sprintf('GIF (PSNR: %.2f)\nSize: %.2f kB ', psnrValuesGIF, imageSizesGIF);
tempImage = imresize(outputImageGIFRGB(50:198,50:198,:),4, 'nearest');
part_image_GIF = insertText(tempImage,[5 5], description, 'FontSize', 24,'BoxOpacity', 0.9);

%%
res_img_2 = cat(2, part_image_BMP,part_image_PNG, part_image_GIF);
res_img = cat(1, res_img_1, res_img_2);
imshow(res_img)

imwrite(res_img,"outputs/res_img_1.jpg","jpg");
