function fsd = dither_better(image)
    input_image = image;

    fsd = zeros(512,512);
    for i = 1:size(input_image ,1)
        for j = 1:size(input_image ,2)
            oldpixel = input_image(i,j);
            newpixel = 0;
            if(oldpixel > 127)
                newpixel = 255;
            else
                newpixel = 0;

            input_image(i,j) = newpixel;
            err = oldpixel - newpixel;

            if(j < size(input_image,2))
                input_image(i,j+1) = input_image(i, j+1) + uint8(7/16*err);
            end
            if(j>1 && i < size(input_image,1))
                input_image(i+1,j-1) = input_image(i+1, j-1) + uint8(3/16*err);
            end
            if(i < size(input_image,1))
                input_image(i+1,j) = input_image(i+1, j) + uint8(5/16*err);
            end
            if(i < size(input_image,1) && j < size(input_image,2))
                input_image(i+1,j+1) = input_image(i+1, j+1) + uint8(1/16*err);
            end
        end
    end
    fsd = input_image;
end