function fsd = dither(image)
    input_image = double(image)/double(max(max(image)));
    brightest = max(max(input_image));
    % brightest = 255;
    p = brightest/2;
    e_tab = zeros(512,512);
    fsd = zeros(512,512);
    for i = 1:size(input_image ,1)
        for j = 1:size(input_image ,2)
            oldpixel = input_image(i,j);
            newpixel = round(oldpixel);

            input_image(i,j) = newpixel;
            err = oldpixel - newpixel;

            if(j < size(input_image,2))
                input_image(i,j+1) = input_image(i, j+1) + 7/16*err;
            end
            if(j>1 && i < size(input_image,1))
                input_image(i+1,j-1) = input_image(i+1, j-1) + 3/16*err;
            end
            if(i < size(input_image,1))
                input_image(i+1,j) = input_image(i+1, j) + 5/16*err;
            end
            if(i < size(input_image,1) && j < size(input_image,2))
                input_image(i+1,j+1) = input_image(i+1, j+1) + 1/16*err;
            end
        end
    end
    fsd = uint8(input_image*double(max(max(image))));
end