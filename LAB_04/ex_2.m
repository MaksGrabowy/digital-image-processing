close all
clear all

I1 = imread("lena_color.bmp");

lena_true_color = add_caption(I1,"Lena true color");

lena_16 = color_quantization(I1,[2 4 2]);

lena_quant_16 = add_caption(lena_16,"Kwantyzacja na 16 barw bez ditheringu");
imwrite(lena_16,"lena_16_quantized.bmp")

lena_16_fs = dither_color(lena_16);

lena_quant_fs_16 = add_caption(lena_16_fs,"Wynik ditheringu - własny skrypt");

[lena_quant_irf_16,map_16] = imread("lena_quantized_16_fs.bmp");
irfan_lena_16 = uint8(ind2rgb(lena_quant_irf_16,map_16)*255);

lena_quant_fs_irf_16 = add_caption(irfan_lena_16,"Wynik ditheringu - irfanview");


row_1 = cat(2,lena_true_color,lena_quant_16,lena_quant_fs_16,lena_quant_fs_irf_16);


lena_256 = color_quantization(I1,[8 8 4]);

lena_quant_256 = add_caption(lena_256,"Kwantyzacja na 256 barw bez ditheringu");
imwrite(lena_256,"lena_256_quantized.bmp")

lena_256_fs = dither_color(lena_256);

lena_quant_fs_256 = add_caption(lena_256_fs,"Wynik ditheringu - własny skrypt");

[lena_quant_irf_256,map_256] = imread("lena_quantized_256_fs.bmp");
irfan_lena_256 = uint8(ind2rgb(lena_quant_irf_256,map_256)*255);

lena_quant_fs_irf_256 = add_caption(irfan_lena_256,"Wynik ditheringu - irfanview");

blank_image = ones(size(lena_true_color))*255;
row_2 = cat(2,blank_image,lena_quant_256,lena_quant_fs_256,lena_quant_fs_irf_256);

figure(1)
result_image_1 = cat(1,row_1,row_2);
imshow(result_image_1);
imwrite(result_image_1,"result_lena.png")

%% image 2
I2 = imread("14_512x512.bmp");
raft_true_color = add_caption(I2,"Raft true color");

raft_16 = color_quantization(I2,[2 4 2]);
raft_quant_16 = add_caption(raft_16,"Kwantyzacja na 16 barw bez ditheringu");
imwrite(raft_16,"raft_16_quantized.bmp")

raft_16_fs = dither_color(raft_16);
raft_quant_fs_16 = add_caption(raft_16_fs,"Wynik ditheringu - własny skrypt");

[raft_quant_irf_16,map_16] = imread("raft_quantized_16_fs.bmp");
irfan_raft_16 = uint8(ind2rgb(raft_quant_irf_16,map_16)*255);
raft_quant_fs_irf_16 = add_caption(irfan_raft_16,"Wynik ditheringu - irfanview");


row_1 = cat(2,raft_true_color,raft_quant_16,raft_quant_fs_16,raft_quant_fs_irf_16);


raft_256 = color_quantization(I2,[8 8 4]);
raft_quant_256 = add_caption(raft_256,"Kwantyzacja na 256 barw bez ditheringu");
imwrite(raft_256,"raft_256_quantized.bmp")

raft_256_fs = dither_color(raft_256);
raft_quant_fs_256 = add_caption(raft_256_fs,"Wynik ditheringu - własny skrypt");

[raft_quant_irf_256,map_256] = imread("raft_quantized_256_fs.bmp");
irfan_raft_256 = uint8(ind2rgb(raft_quant_irf_256,map_256)*255);
raft_quant_fs_irf_256 = add_caption(irfan_raft_256,"Wynik ditheringu - irfanview");

blank_image = ones(size(raft_true_color))*255;
row_2 = cat(2,blank_image,raft_quant_256,raft_quant_fs_256,raft_quant_fs_irf_256);

figure(2)
result_image_2 = cat(1,row_1,row_2);
imshow(result_image_2);
imwrite(result_image_2,"result_raft.png")

psnr_2_1 = psnr(lena_16,I1)
psnr_2_2 = psnr(lena_16_fs,I1)
psnr_2_3 = psnr(irfan_lena_16,I1)
psnr_2_4 = psnr(lena_256,I1)
psnr_2_5 = psnr(lena_256_fs,I1)
psnr_2_6 = psnr(irfan_lena_256,I1)

psnr_3_1 = psnr(raft_16,I2)
psnr_3_2 = psnr(raft_16_fs,I2)
psnr_3_3 = psnr(irfan_raft_16,I2)
psnr_3_4 = psnr(raft_256,I2)
psnr_3_5 = psnr(raft_256_fs,I2)
psnr_3_6 = psnr(irfan_raft_256,I2)