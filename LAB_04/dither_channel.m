% function dithered_channel = dither_channel(channel)
%     brightest = max(max(channel));
%     % brightest = 255;
%     p = brightest/2;
%     e_tab = zeros(512,512);
%     dithered_channel = zeros(512,512);
%     for i = 1:size(channel,1)
%         for j = 1:size(channel,2)
% 
%             if(p>(channel(i,j) + e_tab(i,j)))
%                 dithered_channel(i,j) = 0;
%                 err = channel(i,j)+e_tab(i,j);
%             else
%                 dithered_channel(i,j) = 255;
%                 err = channel(i,j)+e_tab(i,j) - brightest;
%             end
% 
%             if(j < size(channel,2))
%                 e_tab(i,j+1) = e_tab(i, j+1) + (7/16)*err;
%             end
%             if(j>1 && i < size(channel,1))
%                 e_tab(i+1,j-1) = e_tab(i+1, j-1) + (3/16)*err;
%             end
%             if(i < size(channel,1))
%                 e_tab(i+1,j) = e_tab(i+1, j) + (5/16)*err;
%             end
%             if(i < size(channel,1) && j < size(channel,2))
%                 e_tab(i+1,j+1) = e_tab(i+1, j+1) + (1/16)*err;
%             end
%         end
%     end
% end

function dithered_channel = dither_channel(channel)
    
    input_channel = double(channel)/double(max(max(channel)));
    brightest = max(max(input_channel));
    % brightest = 255;
    p = brightest/2;
    e_tab = zeros(512,512);
    dithered_channel = zeros(512,512);
    for i = 1:size(input_channel ,1)
        for j = 1:size(input_channel ,2)

            % if(p>image(i,j) + e_tab(i,j))
            %     fsd(i,j) = 0;
            %     err = image(i,j)+e_tab(i,j);
            % else
            %     fsd(i,j) = 255;
            %     err = image(i,j)+e_tab(i,j) - brightest;
            % end

            oldpixel = input_channel(i,j);
            newpixel = round(oldpixel);

            input_channel(i,j) = newpixel;
            err = oldpixel - newpixel;

            if(j < size(input_channel,2))
                input_channel(i,j+1) = input_channel(i, j+1) + 7/16*err;
            end
            if(j>1 && i < size(input_channel,1))
                input_channel(i+1,j-1) = input_channel(i+1, j-1) + 3/16*err;
            end
            if(i < size(input_channel,1))
                input_channel(i+1,j) = input_channel(i+1, j) + 5/16*err;
            end
            if(i < size(input_channel,1) && j < size(input_channel,2))
                input_channel(i+1,j+1) = input_channel(i+1, j+1) + 1/16*err;
            end
        end
    end
    dithered_channel = uint8(input_channel.*double(max(max(channel))));
end