clear all
close all
I1 = imread("lena.bmp");
I2 = imread("man.bmp");

lena_original = add_caption(I1,"Lena z 256 poziomów szrości");



lena_bin = bin_otsu(I1);
psnr_1_1 = psnr(lena_bin,I1)
lena_otsu = add_caption(lena_bin,"Wynik binaryzacji metodą Otsu");

lena_fs = dither(I1);
psnr_1_2 = psnr(lena_fs,I1)
lena_fs_self = add_caption(lena_fs,"Wynik ditheringu - własny skrypt");

I1_irfan = uint8(imread("lena_dithered_irfan.bmp")*255);
psnr_1_3 = psnr(I1_irfan,I1)
lena_fs_irfan = add_caption(I1_irfan,"Wynik ditheringu - irfanview");

row_1 = cat(2,lena_original,lena_otsu,lena_fs_self,lena_fs_irfan);

man_original = add_caption(I2,"Man z 256 poziomów szrości");

man_bin = bin_otsu(I2);
psnr_1_4 = psnr(man_bin,I2)
man_otsu = add_caption(man_bin,"Wynik binaryzacji metodą Otsu");

man_fs = dither(I2);
psnr_1_5 = psnr(man_fs,I2)
man_fs_self = add_caption(man_fs,"Wynik ditheringu - własny skrypt");

I2_irfan = uint8(imread("man_dithered_irfan.bmp")*255);
psnr_1_6 = psnr(I2_irfan,I2)
man_fs_irfan = add_caption(I2_irfan,"Wynik ditheringu - irfanview");

row_2 = cat(2,man_original,man_otsu,man_fs_self,man_fs_irfan);

res_image = cat(1,row_1,row_2);
imshow(res_image);
imwrite(res_image,"ex1_result.png");