clear all
close all
lena_gr = imread("lena.bmp");
lena_color_256 = imread("lena_256_quantized.bmp");

lena_jjn = dither_jjn(lena_gr);
lena_fs = dither(lena_gr);

lena_gr_captioned = add_caption(lena_gr,"Lena original grayscale");

psnr_fs_gr = psnr(lena_fs,lena_gr);
ssim_fs_gr = ssim(lena_fs,lena_gr);
caption_fs = sprintf("Dithering fs, psnr = %.3f, ssim = %.3f",psnr_fs_gr, ssim_fs_gr);
lena_fs_captioned = add_caption(lena_fs,caption_fs);

psnr_jjn_gr = psnr(lena_jjn,lena_gr);
ssim_jjn_gr = ssim(lena_jjn,lena_gr);
caption_jjn = sprintf("Dithering jjn, psnr = %.3f, ssim = %.3f",psnr_jjn_gr, ssim_jjn_gr);
lena_jjn_captioned = add_caption(lena_jjn,caption_jjn);

%%

lena_jjn_256 = dither_color_jjn(lena_color_256);
lena_fs_256 = dither_color(lena_color_256);

lena_color_256_captioned = add_caption(lena_color_256,"Lena original 256 quantized color");

psnr_fs_256 = psnr(lena_fs_256,lena_color_256);
ssim_fs_256 = ssim(lena_fs_256,lena_color_256);
caption_fs_256 = sprintf("Dithering fs, psnr = %.3f, ssim = %.3f",psnr_fs_256, ssim_fs_256);
lena_fs_captioned_256 = add_caption(lena_fs_256,caption_fs_256);

psnr_jjn_256 = psnr(lena_jjn_256,lena_color_256);
ssim_jjn_256 = ssim(lena_jjn_256,lena_color_256);
caption_jjn_256 = sprintf("Dithering jjn, psnr = %.3f, ssim = %.3f",psnr_jjn_256,ssim_jjn_256);
lena_jjn_captioned_256 = add_caption(lena_jjn_256,caption_jjn_256);

%%
row1 = cat(2,lena_gr_captioned,lena_fs_captioned,lena_jjn_captioned);
row2 = cat(2,lena_color_256_captioned,lena_fs_captioned_256,lena_jjn_captioned_256);

result_image = cat(1,row1,row2);
imwrite(result_image,"ex3_result.png");
figure
imshow(result_image)