function jjnd = dither_jjn(image)
    input_image = double(image)/double(max(max(image)));

    jjnd = zeros(size(image));
    for i = 1:size(input_image ,1)
        for j = 1:size(input_image ,2)
            oldpixel = input_image(i,j);
            newpixel = round(oldpixel);

            input_image(i,j) = newpixel;
            err = oldpixel - newpixel;

            try
            input_image(i,j+1) = input_image(i, j+1) + 7/48*err;
            catch
            end

            try
            input_image(i,j+2) = input_image(i, j+2) + 5/48*err;
            catch
            end
%%%%%%%%%%%%%%%%%%%%
            try
            input_image(i+1,j-2) = input_image(i+1, j-2) + 3/48*err;
            catch
            end

            try
            input_image(i+1,j-1) = input_image(i+1, j-1) + 5/48*err;
            catch
            end

            try
            input_image(i+1,j) = input_image(i+1, j) + 7/48*err;
            catch
            end

            try
            input_image(i+1,j+1) = input_image(i+1, j+1) + 5/48*err;
            catch
            end

            try
            input_image(i+1,j+2) = input_image(i+1, j+2) + 3/48*err;
            catch
            end
%%%%%%%%%%%%%%%%%%%%
            try
            input_image(i+2,j-2) = input_image(i+2, j-2) + 1/48*err;
            catch
            end

            try
            input_image(i+2,j-1) = input_image(i+2, j-1) + 3/48*err;
            catch
            end

            try
            input_image(i+2,j) = input_image(i+2, j) + 5/48*err;
            catch
            end

            try
            input_image(i+2,j+1) = input_image(i+2, j+1) + 3/48*err;
            catch
            end

            try
            input_image(i+2,j+2) = input_image(i+2, j+2) + 1/48*err;
            catch
            end
        end
    end
    jjnd = uint8(input_image*255);
end