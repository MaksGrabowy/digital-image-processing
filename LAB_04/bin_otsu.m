function otsu = bin_otsu(image)
    [counts,x] = imhist(image,256);
    T = otsuthresh(counts);
    otsu = uint8(imbinarize(image,T)*255);
end