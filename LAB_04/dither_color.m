function fsd = dither_color(image)
    ch_r = image(:,:,1);
    ch_g = image(:,:,2);
    ch_b = image(:,:,3);
    dch_r = dither(ch_r);
    dch_g = dither(ch_g);
    dch_b = dither(ch_b);
    fsd = cat(3,dch_r,dch_g,dch_b); 
end
