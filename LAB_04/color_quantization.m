function quantized = color_quantization(image, levels)
    [y_s, x_s, channels] = size(image);
    level_size = floor(256 ./ levels);
    quantized = zeros(size(image), 'uint8');
    for y = 1:y_s
        for x = 1:x_s
            for c = 1:channels
                pixel = floor(double(image(y, x, c)) ./ level_size(c)) .* ...
                level_size(c) + level_size(c) ./ 2;
                quantized(y, x, c) = min(255, pixel); 
            end
        end
    end
end