function captioned = add_caption(image, description)
    [x_size,y_size,channel_size] = size(image);
    bottom = ones(50,x_size,channel_size)*255;
    new_image = cat(1,image,bottom);
    captioned = insertText(new_image,[0 y_size], description, 'FontSize', 24,'BoxOpacity', 0.9);
end
