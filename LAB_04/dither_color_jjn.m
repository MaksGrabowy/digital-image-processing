function jjnd = dither_color_jjn(image)
    ch_r = image(:,:,1);
    ch_g = image(:,:,2);
    ch_b = image(:,:,3);
    dch_r = dither_jjn(ch_r);
    dch_g = dither_jjn(ch_g);
    dch_b = dither_jjn(ch_b);
    jjnd = cat(3,dch_r,dch_g,dch_b); 
end
