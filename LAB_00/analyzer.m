close all 
clear all
image = imread("src_images/minotaur_ubuntu.jpeg");
[indexed, indexed_map] = rgb2ind(image,10);
imwrite(indexed,indexed_map,"minotaur_ind.png")
grayscale_image = rgb2gray(image);
imwrite(grayscale_image,"minotaur_grey.tif")
binary_image = im2bw(image,0.3);
imwrite(binary_image,"minotaur_bw.bmp");


figure
subplot(1,4,1), imshow("src_images/minotaur_ubuntu.jpeg"); title("original")
subplot(1,4,2), imshow("minotaur_ind.png"); title("indexed")
subplot(1,4,3), imshow("minotaur_grey.tif"); title("greyscale")
subplot(1,4,4), imshow("minotaur_bw.bmp"); title("binary")

%% Greyscale conversion
field = imread("src_images/field.jpg");
evangelion = imread("src_images/evangelion.jpeg");
grayscale_field = rgb2gray(field);
grayscale_evangelion = rgb2gray(evangelion);

imwrite(grayscale_field,"field_gray.tif")
imwrite(grayscale_evangelion,"evangelion_gray.tif")

%% Gradient
image_size = 256;
disp(['image size: ', num2str(image_size)]);
gradient_image = zeros(image_size,image_size,'uint8');

for i = 1:image_size
    for j = 1:image_size
        gradient_image(i, j) = uint8((i + j - 2) / 2);
    end
end

figure
imshow(gradient_image);
title('Gradient')
xlabel('x')
ylabel('y')

%% Grayscale calculations

image_1 = imread("field_gray.tif");
image_2 = imread("evangelion_gray.tif");

image_addition = image_1+image_2;
image_substraction = image_1-image_2;
image_mean = (image_1+image_2)/2;

figure
subplot(1,5,1), imshow(image_1); title("I1")
subplot(1,5,2), imshow(image_2); title("I2")
subplot(1,5,3), imshow(image_addition); title("I1+I2")
subplot(1,5,4), imshow(image_substraction); title("I1-I2")
subplot(1,5,5), imshow(uint8(image_mean)); title("Mean(I1,I2)")

%% Channels of an image

minotaur_red = image(:,:,1);
minotaur_green = image(:,:,2);
minotaur_blue = image(:,:,3);

figure
subplot(1,4,1), imshow("src_images/minotaur_ubuntu.jpeg"); title("original")
subplot(1,4,2), imshow(minotaur_red); title("red")
subplot(1,4,3), imshow(minotaur_green); title("green")
subplot(1,4,4), imshow(minotaur_blue); title("blue")

minotaur_black = zeros(size(image, 1), size(image, 2), 'uint8');

just_red = cat(3, minotaur_red, minotaur_black, minotaur_black);
just_green = cat(3, minotaur_black, minotaur_green, minotaur_black);
just_blue = cat(3, minotaur_black, minotaur_black, minotaur_blue);

figure
subplot(1,4,1), imshow("src_images/minotaur_ubuntu.jpeg"); title("original")
subplot(1,4,2), imshow(just_red); title("red")
subplot(1,4,3), imshow(just_green); title("green")
subplot(1,4,4), imshow(just_blue); title("blue")

figure
subplot(2,4,1), imshow("src_images/minotaur_ubuntu.jpeg"); title("original")
subplot(2,4,2), imshow(minotaur_red); title("red")
subplot(2,4,3), imshow(minotaur_green); title("green")
subplot(2,4,4), imshow(minotaur_blue); title("blue")

subplot(2,4,5), imshow("src_images/minotaur_ubuntu.jpeg"); title("original")
subplot(2,4,6), imshow(just_red); title("red")
subplot(2,4,7), imshow(just_green); title("green")
subplot(2,4,8), imshow(just_blue); title("blue")

%% Channels HSV

image_hsv = rgb2hsv(image);

minotaur_red_hsv = image_hsv(:,:,1);
minotaur_green_hsv = image_hsv(:,:,2);
minotaur_blue_hsv = image_hsv(:,:,3);

figure
subplot(1,4,1), imshow(image_hsv); title("original")
subplot(1,4,2), imshow(minotaur_red_hsv); title("red")
subplot(1,4,3), imshow(minotaur_green_hsv); title("green")
subplot(1,4,4), imshow(minotaur_blue_hsv); title("blue")

minotaur_black_hsv = zeros(size(image_hsv, 1), size(image_hsv, 2));

just_red_hsv = cat(3, minotaur_red_hsv, minotaur_black_hsv, minotaur_black_hsv);
just_green_hsv = cat(3, minotaur_black_hsv, minotaur_green_hsv, minotaur_black_hsv);
just_blue_hsv = cat(3, minotaur_black_hsv, minotaur_black_hsv, minotaur_blue_hsv);

figure
subplot(1,4,1), imshow(image_hsv); title("original")
subplot(1,4,2), imshow(just_red_hsv); title("red")
subplot(1,4,3), imshow(just_green_hsv); title("green")
subplot(1,4,4), imshow(just_blue_hsv); title("blue")

figure
subplot(2,4,1), imshow(image_hsv); title("original")
subplot(2,4,2), imshow(minotaur_red_hsv); title("H")
subplot(2,4,3), imshow(minotaur_green_hsv); title("S")
subplot(2,4,4), imshow(minotaur_blue_hsv); title("V")

subplot(2,4,5), imshow(image_hsv); title("original")
subplot(2,4,6), imshow(just_red_hsv); title("red")
subplot(2,4,7), imshow(just_green_hsv); title("green")
subplot(2,4,8), imshow(just_blue_hsv); title("blue")

%% Channels Ycbcr

image_ycbcr = rgb2ycbcr(image);

minotaur_red_ycbcr = image_ycbcr(:,:,1);
minotaur_green_ycbcr = image_ycbcr(:,:,2);
minotaur_blue_ycbcr = image_ycbcr(:,:,3);

figure
subplot(1,4,1), imshow(image_ycbcr); title("original")
subplot(1,4,2), imshow(minotaur_red_ycbcr); title("red")
subplot(1,4,3), imshow(minotaur_green_ycbcr); title("green")
subplot(1,4,4), imshow(minotaur_blue_ycbcr); title("blue")

minotaur_black_ycbcr = zeros(size(image_ycbcr, 1), size(image_ycbcr, 2));

just_red_ycbcr = cat(3, minotaur_red_ycbcr, minotaur_black_ycbcr, minotaur_black_ycbcr);
just_green_ycbcr = cat(3, minotaur_black_ycbcr, minotaur_green_ycbcr, minotaur_black_ycbcr);
just_blue_ycbcr = cat(3, minotaur_black_ycbcr, minotaur_black_ycbcr, minotaur_blue_ycbcr);

figure
subplot(1,4,1), imshow(image_ycbcr); title("original")
subplot(1,4,2), imshow(just_red_ycbcr); title("red")
subplot(1,4,3), imshow(just_green_ycbcr); title("green")
subplot(1,4,4), imshow(just_blue_ycbcr); title("blue")

figure
subplot(2,4,1), imshow(image_ycbcr); title("original")
subplot(2,4,2), imshow(minotaur_red_ycbcr); title("H")
subplot(2,4,3), imshow(minotaur_green_ycbcr); title("S")
subplot(2,4,4), imshow(minotaur_blue_ycbcr); title("V")

subplot(2,4,5), imshow(image_ycbcr); title("original")
subplot(2,4,6), imshow(just_red_ycbcr); title("red")
subplot(2,4,7), imshow(just_green_ycbcr); title("green")
subplot(2,4,8), imshow(just_blue_ycbcr); title("blue")


%% Histograms

I_normal = imread("evangelion_gray.tif");
I_high = imadjust(I_normal,[0.1 0.4],[]);
I_low = imadjust(I_normal,[0.3 1.0],[]);
figure
subplot(3,2,1), imshow(I_normal);
subplot(3,2,3), imshow(I_high); 
subplot(3,2,5), imshow(I_low); 

subplot(3,2,2), imhist(I_normal);
subplot(3,2,4), imhist(I_high); 
subplot(3,2,6), imhist(I_low); 

%% Stretching

I1 = imread("evangelion_gray.tif");
I1s = imadjust(I1);

I2 = imread("field_gray.tif");
I2s = imadjust(I2);

figure
subplot(2,2,1), imshow(I1); title("I1")
subplot(2,2,2), imshow(I1s); 
subplot(2,2,3), imhist(I1);
subplot(2,2,4), imhist(I1s);

figure
subplot(2,2,1), imshow(I2); title("I2")
subplot(2,2,2), imshow(I2s); 
subplot(2,2,3), imhist(I2);
subplot(2,2,4), imhist(I2s);

%histeq

I1e = histeq(I1);
I2e = histeq(I2);

figure
subplot(2,3,1), imshow(I1); title("I1")
subplot(2,3,2), imshow(I1e); title("I1e")
subplot(2,3,3), imshow(I1s); title("I1s")
subplot(2,3,4), imhist(I1);
subplot(2,3,5), imhist(I1e);
subplot(2,3,6), imhist(I1s);

figure
subplot(2,3,1), imshow(I2); title("I2")
subplot(2,3,2), imshow(I2e); title("I2e")
subplot(2,3,3), imshow(I2s); title("I2s")
subplot(2,3,4), imhist(I2);
subplot(2,3,5), imhist(I2e);
subplot(2,3,6), imhist(I2s);


