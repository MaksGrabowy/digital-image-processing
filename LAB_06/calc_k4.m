function k4 = calc_k4(image)

    [M,N]=size(image);
    l_avg = double(sum(sum(image))/(M*N));

    norm_sum = 0.0;
    for i = 1:M
        for j = 1:N
            norm_sum = norm_sum + double((image(i,j)-l_avg))^2;
        end
    end

    k4 = (4/(255*255*M*N))*norm_sum;
end

