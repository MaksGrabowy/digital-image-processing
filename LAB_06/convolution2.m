function result = convolution2(A,B)
    [y_a, x_a] = size(A);
    [y_b, x_b] = size(B);
    pad_height = floor(y_b/2);
    pad_width = floor(x_b/2);

    side_pad_left = flip(A(1:end,1:pad_width),2);
    side_pad_right = flip(A(1:end,end-pad_width+1:end),2);

    semi_pad = cat(2,side_pad_left,A,side_pad_right);

    mid_pad_top = flip(semi_pad(1:pad_height,1:end),1);
    mid_pad_bottom = flip(semi_pad(end-pad_height+1:end,1:end),1);

    padded_a = cat(1,mid_pad_top,semi_pad,mid_pad_bottom);
    orig_x = pad_width+1;
    orig_y = pad_height+1;
    for x = 1:x_a
        for y = 1:y_a
            patch = padded_a(orig_y+y-1-pad_height:orig_y+y-1+pad_height,orig_x+x-1-pad_width:orig_x+x-1+pad_width);

            result(y,x) = uint8(sum(sum(double(patch).*B)));
        end
    end
end

