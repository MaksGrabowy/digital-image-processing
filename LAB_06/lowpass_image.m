function result = lowpass_image(image,mask)
    matrix_sum = sum(sum(mask));
    mask = mask./matrix_sum;
    result = convolution2(image,mask);
    % result = uint8(conv2(image,mask,"same"));
end

