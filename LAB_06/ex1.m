clear all
close all

I_in = imread("Henckel.jpg");

% I_in = checkerboard(20);

I1 = im2gray(I_in);

mask_lp = ones(13,27);

mask_hp_3x3 = [0 -1 0;
    -1 5 -1;
    0 -1 0];

mask_hp_5x3 = [0 0 -1 0 0;
    -1 -1 7 -1 -1;
    0 0 -1 0 0];
i1_low = lowpass_image(I1,mask_lp);
i1_high = highpass_image(I1,mask_hp_5x3);


subplot(2,3,1)
imshow(I1)
title(sprintf("Wielkość obrazu O1 : %d x %d",size(I1,1),size(I1,2)));

subplot(2,3,2)
imshow(i1_low)
title(sprintf("Wielkość obrazu : %d x %d\nWielkość maski: %d x %d",size(i1_low,2),size(i1_low,1),size(mask_lp,2),size(mask_lp,1)));

subplot(2,3,3)
imshow(i1_high)
title(sprintf("Wielkość obrazu : %d x %d\nWielkość maski: %d x %d",size(i1_high,2),size(i1_high,1),size(mask_hp_5x3,2),size(mask_hp_5x3,1)));

I_in = imread("Ex2.png");
I2 = im2gray(I_in);

i2_low = lowpass_image(I2,mask_lp);
i2_high = highpass_image(I2,mask_hp_5x3);

subplot(2,3,4)
imshow(I2)
title(sprintf("Wielkość obrazu O2 : %d x %d",size(I2,1),size(I2,2)));

subplot(2,3,5)
imshow(i2_low)
title(sprintf("Wielkość obrazu : %d x %d\nWielkość maski: %d x %d",size(i2_low,2),size(i2_low,1),size(mask_lp,2),size(mask_lp,1)));

subplot(2,3,6)
imshow(i2_high)
title(sprintf("Wielkość obrazu : %d x %d\nWielkość maski: %d x %d",size(i2_high,2),size(i2_high,1),size(mask_hp_5x3,2),size(mask_hp_5x3,1)));

%% Side by side comparisons

i1_comparison_low = cat(2,I1,i1_low);
figure
imshow(i1_comparison_low);

i1_comparison_high = cat(2,I1,i1_high);
figure
imshow(i1_comparison_high);


i2_comparison_low = cat(2,I2,i2_low);
figure
imshow(i2_comparison_low);

i2_comparison_high = cat(2,I2,i2_high);
figure
imshow(i2_comparison_high);

imwrite(i1_comparison_low,"comparison_low_1.png")
imwrite(i1_comparison_high,"comparison_high_1.png")
imwrite(i2_comparison_low,"comparison_low_2.png")
imwrite(i2_comparison_high,"comparison_high_2.png")