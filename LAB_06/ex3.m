close all
clear all
I = im2gray(imread("Ex1.png"));

mask_lp = ones(5,5);

mask_hp_3x3 = [0 -1 0;
    -1 5 -1;
    0 -1 0];

mask_hp_5x3 = [0 0 -1 0 0;
    -1 -1 7 -1 -1;
    0 0 -1 0 0];

mask_hp_5x5 = [0 0 -1 0 0;
    0 -1 -1 -1 0;
    -1 -1 13 -1 -1;
     0 -1 -1 -1 0;
    0 0 -1 0 0];

mask_hp_5x7 = [0 0 -1 0 0;
    0 0 -1 0 0;
    0 -1 -2 -1 0;
    -1 -2 19.2 -2 -1;
     0 -1 -2 -1 0;
     0 0 -1 0 0;
    0 0 -1 0 0];

blurred = lowpass_image(I,mask_lp);
amount = 5;

sharpened = I + (I-blurred)*amount;

subplot(2,2,1)
imshow(I);
title(sprintf("Ocena kontrastu k4 : %.2f",calc_k4(I)));

subplot(2,2,2)
imshow(sharpened);
title(sprintf("Ocena kontrastu k4 : %.2f",calc_k4(sharpened)));

clahe_im = adapthisteq(sharpened,"NumTiles",[16,16],"ClipLimit",0.05,"Nbins",256,"Range","full","Distribution","rayleigh","Alpha",0.4);
subplot(2,2,3)
imshow(clahe_im)
title(sprintf("Ocena kontrastu k4 : %.2f",calc_k4(clahe_im)));

eq_im = histeq(sharpened);

subplot(2,2,4)
imshow(eq_im)
title(sprintf("Ocena kontrastu k4 : %.2f",calc_k4(eq_im)));
