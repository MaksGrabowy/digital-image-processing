close all
clear all
% I = im2gray(imread("Henckel.jpg"));
I = im2gray(imread("Ex2.png"));

I_noise = imnoise(I,"gaussian");


mask_lp = ones(15,7);

mask_hp_3x3 = [0 -1 0;
    -1 5 -1;
    0 -1 0];

mask_hp_5x3 = [0 0 -1 0 0;
    -1 -1 7 -1 -1;
    0 0 -1 0 0];

mask_hp_5x5 = [0 0 -1 0 0;
    0 -1 -1 -1 0;
    -1 -1 13 -1 -1;
     0 -1 -1 -1 0;
    0 0 -1 0 0];

mask_hp_5x7 = [0 0 -1 0 0;
    0 0 -1 0 0;
    0 -1 -2 -1 0;
    -1 -2 19.2 -2 -1;
     0 -1 -2 -1 0;
     0 0 -1 0 0;
    0 0 -1 0 0];


Id = lowpass_image(I_noise,mask_lp);

Ig = highpass_image(Id,mask_hp_5x7);

O_id = I-Id;
O_i = I-Ig;

subplot(2,3,1)
imshow(I)
title(sprintf("Wielkość obrazu O1 : %d x %d",size(I,1),size(I,2)));

subplot(2,3,2)
imshow(Id)
title(sprintf("Wielkość obrazu : %d x %d\nWielkość maski: %d x %d",size(Id,2),size(Id,1),size(mask_lp,2),size(mask_lp,1)));

subplot(2,3,3)
imshow(Ig)
psnr_g = psnr(Ig,I);
title(sprintf("Wielkość obrazu : %d x %d\nWielkość maski: %d x %d\nPSNR : %.2f",size(Ig,2),size(Ig,1),size(mask_hp_5x7,2),size(mask_hp_5x7,1),psnr_g));

subplot(2,3,4)
imshow(I_noise)

subplot(2,3,5)
imshow(O_id)

subplot(2,3,6)
imshow(O_i)

%% comparisons

row_1 = cat(2, I, Id, Ig);
row_2 = cat(2, I_noise, O_i, O_id);
whole = cat(1, row_1,row_2);
figure
imshow(whole)
imwrite(whole,"comparison_ex2_2.png");