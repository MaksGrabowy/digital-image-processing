function i_res = stretcher_crop(image, thresh)

    [counts,vals] = imhist(image);
    for i = 1:256
        if(counts(i) >= thresh)
            gr_min = i;
            break;
        end
        
    end

    for i = 256:-1:1
        if(counts(i) >= thresh)
            gr_max = i;
            break;
        end
        
    end

    for i = 1:size(image,1)
        for j = 1:size(image,2)
            i_res(i, j) = ((255)/(gr_max-gr_min))*(image(i, j)-gr_min)+0;
        end
    end
end

