function i_res = stretcher(image)
    gr_max = max(max(image));
    gr_min = min(min(image));
    for i = 1:size(image,1)
        for j = 1:size(image,2)
            i_res(i, j) = ((255)/(gr_max-gr_min))*(image(i, j)-gr_min)+0;
        end
    end
end

