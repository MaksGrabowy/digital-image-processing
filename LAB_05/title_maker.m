function [k1,k2,k3,k4,title] = title_maker(image)
    l_max = double(max(max(image)));
    l_min = double(min(min(image)));
    k1 = (l_max-l_min)/255;

    [M,N]=size(image);
    l_avg = double(sum(sum(image))/(M*N));
    k2 = (l_max-l_min)/l_avg;

    k3 = (l_max-l_min)/(l_min+l_max);

    norm_sum = 0.0;
    for i = 1:M
        for j = 1:N
            norm_sum = norm_sum + double((image(i,j)-l_avg))^2;
        end
    end

    k4 = (4/(255*255*M*N))*norm_sum;

    title = sprintf("k1 = %.4f, k2 = %.4f, k3 = %.4f, k4 = %.4f\nmin(Ox) = %d, max(Ox) = %d",k1,k2,k3,k4,l_min,l_max);

end

