clear all
close all
I_in = imread("Ex2.png"); %this image is 800x1200 px so dividing it into 20x30 parts is easy

I1 = im2gray(I_in);

[~,~,~,~,title_1] = title_maker(I1);

subplot(2,3,1)
imshow(I1)
title(title_1)

i_stretch_global = stretcher_crop(I1,500);
[~,~,~,~,title_2] = title_maker(i_stretch_global);
subplot(2,3,2)
imshow(i_stretch_global)
title(title_2)

i_stretch_local = stretch_local(I1,10,160,240);
[~,~,~,~,title_3] = title_maker(i_stretch_local);
subplot(2,3,3)
imshow(i_stretch_local)
title(title_3)

subplot(2,3,4)
imhist(I1)

subplot(2,3,5)
imhist(i_stretch_global)

subplot(2,3,6)
imhist(i_stretch_local)