function i_result = stretch_local(image, thresh,y_size,x_size)
    [size_y,size_x] = size(image);
    for i = 1:y_size:size_y
        for j = 1:x_size:size_x
            patch = image(i:i+y_size-1,j:j+x_size-1);
            stretched_patch = stretcher_crop(patch,thresh);
            i_result(i:i+y_size-1,j:j+x_size-1) = stretched_patch;
        end
    end
end

