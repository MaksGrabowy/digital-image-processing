clear all
close all
I_in = imread("Ex2.png");

I1 = im2gray(I_in);

[~,~,~,~,title_1] = title_maker(I1);

subplot(2,3,1)
imshow(I1)
title(title_1)

i_stretch = stretcher_crop(I1,800);
[~,~,~,~,title_2] = title_maker(i_stretch);
subplot(2,3,2)
imshow(i_stretch)
title(title_2)

ieq = histeq(I1);
[~,~,~,~,title_3] = title_maker(ieq);
subplot(2,3,3)
imshow(ieq)
title(title_3)

subplot(2,3,4)
imhist(I1)

subplot(2,3,5)
imhist(i_stretch)

subplot(2,3,6)
imhist(ieq)