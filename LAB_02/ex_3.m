close all;
clear all;
picture_size = 120:220;

im_1_cfa = im2double(imread('CFA_sRGB/IMG_013_srgb_CFA.png'));
im_source_1_raw = imread('CFA_sRGB/IMG_013_srgb_CFA.png');
im_source_1_rgb = cat(3, im_1_cfa,im_1_cfa, im_1_cfa);

imshow(im_source_1_rgb)

im_source_1_raw = imresize(im_source_1_raw(picture_size,picture_size,:),4, 'nearest');
im_source_1_raw = insertText(im_source_1_raw,[5 5],"obraz surowy 1", 'FontSize', 24,'BoxOpacity', 0.0,'FontColor','white');

im_orig_1 = imread("GT_sRGB/IMG_013_srgb.png");
im_original_1 = imread("GT_sRGB/IMG_013_srgb.png");

im_orig_1 = imresize(im_orig_1(picture_size,picture_size,:),4, 'nearest');
im_orig_1 = insertText(im_orig_1,[5 5],"oryginał 1", 'FontSize', 24,'BoxOpacity', 0.0,'FontColor','white');

Rch = repmat ([1 0;0 0] , size (im_1_cfa) /2) ;
Gch = repmat ([0 1;1 0] , size (im_1_cfa) /2) ;
Bch = repmat ([0 0;0 1] , size (im_1_cfa) /2) ;

Rh = im_1_cfa .* Rch;
Gh = im_1_cfa .* Gch;
Bh = im_1_cfa .* Bch;

R = conv2 (Rh , [1 2 1; 2 4 2; 1 2 1]/4 ) ;
G = conv2 (Gh , [0 1 0; 1 4 1; 0 1 0]/4 ) ;
B = conv2 (Bh , [1 2 1; 2 4 2; 1 2 1]/4 ) ;

im_1_res = cat(3, B, G, R);
im_1_res = im2uint8(im_1_res);
im_1_res = im_1_res(1:288,1:432,:);
psnr_1 = psnr(im_1_res, im_original_1);
im_1_res = imresize(im_1_res(picture_size,picture_size,:),4, 'nearest');


text = sprintf("biliniowa 1 PSNR %.2f",psnr_1);
im_1_res = insertText(im_1_res,[5 5],text, 'FontSize', 24,'BoxOpacity', 0.0,'FontColor','white');

im_row_1 = cat(2,im_source_1_raw,im_1_res,im_orig_1);
% im_row_2 = cat(2,I_orig_2_raw,im_2,I_orig_2)
% im_res = cat(1,im_row_1, im_row_2)
% imshow(im_row_1)

%%
im_2_cfa = im2double(imread('CFA_sRGB/IMG_008_srgb_CFA.png'));
im_source_2_raw = imread('CFA_sRGB/IMG_008_srgb_CFA.png');
im_source_2_rgb = cat(3, im_2_cfa,im_2_cfa, im_2_cfa);

imshow(im_source_2_rgb)

im_source_2_raw = imresize(im_source_2_raw(picture_size,picture_size,:),4, 'nearest');
im_source_2_raw = insertText(im_source_2_raw,[5 5],"obraz surowy 2", 'FontSize', 24,'BoxOpacity', 0.0,'FontColor','white');

im_orig_2 = imread("GT_sRGB/IMG_008_srgb.png");
im_original_2 = imread("GT_sRGB/IMG_008_srgb.png");

im_orig_2 = imresize(im_orig_2(picture_size,picture_size,:),4, 'nearest');
im_orig_2 = insertText(im_orig_2,[5 5],"oryginał 2", 'FontSize', 24,'BoxOpacity', 0.0,'FontColor','white');

Rch = repmat ([1 0;0 0] , size (im_2_cfa) /2) ;
Gch = repmat ([0 1;1 0] , size (im_2_cfa) /2) ;
Bch = repmat ([0 0;0 1] , size (im_2_cfa) /2) ;

Rh = im_2_cfa .* Rch;
Gh = im_2_cfa .* Gch;
Bh = im_2_cfa .* Bch;

R = conv2 (Rh , [1 2 1; 2 4 2; 1 2 1]/4 ) ;
G = conv2 (Gh , [0 1 0; 1 4 1; 0 1 0]/4 ) ;
B = conv2 (Bh , [1 2 1; 2 4 2; 1 2 1]/4 ) ;

im_2_res = cat(3, B, G, R);
im_2_res = im2uint8(im_2_res);
im_2_res = im_2_res(1:288,1:432,:);
psnr_2 = psnr(im_2_res, im_original_2);
im_2_res = imresize(im_2_res(picture_size,picture_size,:),4, 'nearest');


text = sprintf("biliniowa 2 PSNR %.2f",psnr_2);
im_2_res = insertText(im_2_res,[5 5],text, 'FontSize', 24,'BoxOpacity', 0.0,'FontColor','white');

im_row_2 = cat(2,im_source_2_raw,im_2_res,im_orig_2);
% im_row_2 = cat(2,I_orig_2_raw,im_2,I_orig_2)
% im_res = cat(1,im_row_1, im_row_2)
% imshow(im_row_2)

%%
im_result = cat(1, im_row_1, im_row_2);
imshow(im_result);

imwrite(im_result,"ex_3_result.jpg","jpg");
