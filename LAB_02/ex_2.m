clear all
close all
picture_size = 120:220;
I_source_1 = imread("CFA_sRGB/IMG_013_srgb_CFA.png");
I_orig_1_raw = imresize(I_source_1(picture_size,picture_size,:),4, 'nearest');
I_orig_1_raw = insertText(I_orig_1_raw,[5 5],"obraz surowy 1", 'FontSize', 24,'BoxOpacity', 0.0,'FontColor','white');

I_orig_1 = imread("GT_sRGB/IMG_013_srgb.png");

I_orig_1 = imresize(I_orig_1(picture_size,picture_size,:),4, 'nearest');
I_orig_1 = insertText(I_orig_1,[5 5],"oryginał 1", 'FontSize', 24,'BoxOpacity', 0.0,'FontColor','white');
im_source_1_rgb = cat(3,I_source_1,I_source_1,I_source_1);


I_source_2 = imread("CFA_sRGB/IMG_008_srgb_CFA.png");
I_orig_2_raw = imresize(I_source_2(picture_size,picture_size,:),4, 'nearest');
I_orig_2_raw = insertText(I_orig_2_raw,[5 5],"obraz surowy 2", 'FontSize', 24,'BoxOpacity', 0.0,'FontColor','white');

I_orig_2 = imread("GT_sRGB/IMG_008_srgb.png");
I_orig_2 = imresize(I_orig_2(picture_size,picture_size,:),4, 'nearest');
I_orig_2 = insertText(I_orig_2,[5 5],"oryginał 2", 'FontSize', 24,'BoxOpacity', 0.0,'FontColor','white');

im_source_2_rgb = cat(3,I_source_2,I_source_2,I_source_2);

% result_image = cat(3,zeros(100),zeros(100),zeros(100))
%%BGGR images
for i = 1:2:size(I_source_1,1)
    for j = 1:2:size(I_source_1,2)
        cropped = I_source_1(i:i+1,j:j+1);
        B1 = cropped(1,1);
        G1 = cropped(1,2);
        G2 = cropped(2,1);
        R1 = cropped(2,2);
        R_s = [R1 R1; R1 R1];
        G_s = [G1 G1;G2 G2];
        B_s = [B1 B1;B1 B1];
        res_s = cat(3,R_s,G_s,B_s);
        result_image_1(i:i+1,j:j+1,:) = res_s;
    end
end


im_1 = imresize(result_image_1(picture_size,picture_size,:),4, 'nearest');
psnr_im_1 = psnr(result_image_1,im_source_1_rgb);
desc = sprintf("NN1 PSNR %.2f",psnr_im_1);
im_1 = insertText(im_1,[5 5],desc, 'FontSize', 24,'BoxOpacity', 0.0,'FontColor','white');

% figure(1)
% imshow(im_1)

for i = 1:2:size(I_source_2,1)
    for j = 1:2:size(I_source_2,2)
        cropped = I_source_2(i:i+1,j:j+1);
        B1 = cropped(1,1);
        G1 = cropped(1,2);
        G2 = cropped(2,1);
        R1 = cropped(2,2);
        R_s = [R1 R1; R1 R1];
        G_s = [G1 G1;G2 G2];
        B_s = [B1 B1;B1 B1];
        res_s = cat(3,R_s,G_s,B_s);
        result_image_2(i:i+1,j:j+1,:) = res_s;
    end
end

im_2 = imresize(result_image_2(picture_size,picture_size,:),4, 'nearest');
psnr_im_2 = psnr(result_image_2,im_source_2_rgb);
desc = sprintf("NN2 PSNR %.2f",psnr_im_2);
im_2 = insertText(im_2,[5 5],desc, 'FontSize', 24,'BoxOpacity', 0.0,'FontColor','white');

figure(2)

im_row_1 = cat(2,I_orig_1_raw,im_1,I_orig_1);
im_row_2 = cat(2,I_orig_2_raw,im_2,I_orig_2);
im_res = cat(1,im_row_1, im_row_2);
imshow(im_res);

imwrite(im_res,"ex_2_result.jpg","jpg");

