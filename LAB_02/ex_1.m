clear all;
close all;

I = imread("GT_sRGB/IMG_013_srgb.png");
Icfa = imread("CFA_sRGB/IMG_013_srgb_CFA.png");

picture_size = 120:220;
im_original =  imresize(I(picture_size,picture_size,:),4, "nearest");
im_original = insertText(im_original,[5 5],"obraz GT", "FontSize", 24,"BoxOpacity", 0.0,"FontColor","white");

im_cfa = imresize(Icfa(picture_size,picture_size,:),4, "nearest");
im_cfa = insertText(im_cfa,[5 5],"obraz surowy", "FontSize", 24,"BoxOpacity", 0.0,"FontColor","white");
% im_cfa_rgb = cat(3, im_cfa, im_cfa, im_cfa);
I_cfa_gbrg = demosaic(Icfa,"gbrg");
I_cfa_grbg = demosaic(Icfa,"grbg");
I_cfa_bggr = demosaic(Icfa,"bggr");
I_cfa_rggb = demosaic(Icfa,"rggb");
I1 = imresize(I_cfa_gbrg(picture_size,picture_size,:),4, "nearest");
I1 = insertText(I1,[5 5],"układ gbrg","FontSize", 24,"BoxOpacity", 0.0,"FontColor","white");
I2 = imresize(I_cfa_grbg(picture_size,picture_size,:),4, "nearest");
I2 = insertText(I2,[5 5],"układ grbg", "FontSize", 24,"BoxOpacity", 0.0,"FontColor","white");
I3 = imresize(I_cfa_bggr(picture_size,picture_size,:),4, "nearest");
I3 = insertText(I3,[5 5],"układ bggr", "FontSize", 24,"BoxOpacity", 0.0,"FontColor","white");
I4 = imresize(I_cfa_rggb(picture_size,picture_size,:),4, "nearest");
I4 = insertText(I4,[5 5],"układ rggb", "FontSize", 24,"BoxOpacity", 0.0,"FontColor","white");
Im_p1 = cat(2,im_cfa, I1, I2);
Im_p2 = cat(2,im_original, I3, I4);
im_res = cat(1, Im_p1,Im_p2);
figure(1)
imshow(im_res)

imwrite(im_res,"ex_1_result_1.jpg","jpg");

I = imread("GT_sRGB/IMG_008_srgb.png");
Icfa = imread("CFA_sRGB/IMG_008_srgb_CFA.png");

picture_size = 120:220;
im_original =  imresize(I(picture_size,picture_size,:),4, "nearest");
im_original = insertText(im_original,[5 5],"obraz GT","FontSize", 24,"BoxOpacity", 0.0,"FontColor","white");

im_cfa = imresize(Icfa(picture_size,picture_size,:),4, "nearest");
im_cfa = insertText(im_cfa,[5 5],"obraz surowy","FontSize", 24,"BoxOpacity", 0.0,"FontColor","white");
% im_cfa_rgb = cat(3, im_cfa, im_cfa, im_cfa);
I_cfa_gbrg = demosaic(Icfa,"gbrg");
I_cfa_grbg = demosaic(Icfa,"grbg");
I_cfa_bggr = demosaic(Icfa,"bggr");
I_cfa_rggb = demosaic(Icfa,"rggb");
I1 = imresize(I_cfa_gbrg(picture_size,picture_size,:),4, "nearest");
I1 = insertText(I1,[5 5],"układ gbrg", "FontSize", 24,"BoxOpacity", 0.0,"FontColor","white");
I2 = imresize(I_cfa_grbg(picture_size,picture_size,:),4, "nearest");
I2 = insertText(I2,[5 5],"układ grbg", "FontSize", 24,"BoxOpacity", 0.0,"FontColor","white");
I3 = imresize(I_cfa_bggr(picture_size,picture_size,:),4, "nearest");
I3 = insertText(I3,[5 5],"układ bggr",  "FontSize", 24,"BoxOpacity", 0.0,"FontColor","white");
I4 = imresize(I_cfa_rggb(picture_size,picture_size,:),4, "nearest");
I4 = insertText(I4,[5 5],"układ rggb",  "FontSize", 24,"BoxOpacity", 0.0,"FontColor","white");
Im_p1 = cat(2,im_cfa, I1, I2);
Im_p2 = cat(2,im_original, I3, I4);
im_res = cat(1, Im_p1,Im_p2);

figure(2)
imshow(im_res)

imwrite(im_res,"ex_1_result_2.jpg","jpg");
